import React from "react";

import "./styles.css";

import  Home  from "../../assets/ImagesGlobal/home.svg";
import Arrow from "../../assets/ImagesGlobal/arrow.svg";

const BreadCrumbs = () => {
    
    return (
        <div className="wrapper-breadcrumbs" >
            <img className="breadcrumbs-home" src={ Home } alt="Home" />
            <img className="breadcrumbs-arrow" src={ Arrow } alt="Arrow" />
            <p>institucional</p>
    
        </div>
    )
}   



export { BreadCrumbs };