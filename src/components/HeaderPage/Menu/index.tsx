import React from "react";
import "./styles.css";

const Menu: React.FC = () => {
  return (
    <div className="header-menu-container">
      <div className="header-menu-nav">
        <div className="header-menu-content">
          <button className="header-menu-button-cursos">cursos</button>
          <button className="header-menu-button-saibamais">saiba mais</button>
        </div>
      </div>
    </div>
  );
};

export { Menu };
