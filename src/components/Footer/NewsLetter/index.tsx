import React from "react";

import { Formik, Form, Field, ErrorMessage } from "formik";
import NewsletterSchema from "../../../schema/NewsletterSchema";

import "./styles.css";

import { Whatsapp } from "../Whatsapp/index";



interface IFormikValues {
    email: string;
  }
  
  const initialValues = {
    email: ""
  };
  




const NewsletterForm = () => {
    const submitNewsletter = (values: IFormikValues) => {
        console.log(values);
}

return (

<div className="container-newsletter">

        <Formik
          onSubmit={submitNewsletter}
          initialValues={initialValues}
          validationSchema={NewsletterSchema}
        >
          {({ errors, touched }) => (

            <Form>

                <div className="newsletter-title">
                <h4 className="newsletter-text">assine nossa newsletter</h4>
                </div>

                <div className="newsletter-input-container" >
              <div className="container-input" >
              <Field
                id="newsletter"
                name="newsletter"
                placeholder="E-mail"
                className={errors.email && touched.email && "invalid"}
              />
              <ErrorMessage
                component="span"
                name="newsletter"
                className="newsletter-invalid-feedback"
              />
              </div>
              <button className="newsletter-input-button" type="submit">
                Enviar
              </button>
              </div>
            </Form>
          )}

        </Formik>
            
        <Whatsapp/>
            
      </div>
            

          );
        }


export { NewsletterForm };