import React from "react";

import "./styles.css";

import WhatsappImg from "../../../assets/ImagesFooter/whatsapp.svg";
import { ScrollTop } from "../../ScrollTop/index"

const Whatsapp = () => {
    
    return (
        <div className="contact-wrapper">
          <div className="contact-icons">

            <div className="flex align-end">
              <a href="https://www.whatsapp.com/?lang=pt_br"><img className="img-whatsapp" src={WhatsappImg} alt="Whatsapp" /></a>
            </div>

            <div className="flex align-end">
              <ScrollTop/>
            </div>

          </div>
        </div>
    )
};


export { Whatsapp };