import React from "react";

import "./styles.css";

import Facebook from "../../../assets/ImagesFooter/facebook.svg";
import Instagram from "../../../assets/ImagesFooter/instagram.svg";
import Twitter from "../../../assets/ImagesFooter/twitter.svg";
import Youtube from "../../../assets/ImagesFooter/youtube.svg";
import Linkedin from "../../../assets/ImagesFooter/linkedin.svg";
import { Copyright } from "../FooterCopyright";

const Information = () => {
  return (
    <div className="wrapper-informacoes">
      <div className="menu-footer">
        <div className="menu-wrapper">
          <p>Institucional</p>
          <img
            src={require("../../../assets/ImagesFooter/sum.png")}
            alt="Ícone + "
          />
        </div>
        <div className="menu-wrapper">
          <p>Dúvidas</p>
          <img
            src={require("../../../assets/ImagesFooter/sum.png")}
            alt="Ícone + "
          />
        </div>
        <div className="menu-wrapper">
          <p>Fale Conosco</p>
          <img
            src={require("../../../assets/ImagesFooter/sum.png")}
            alt="Ícone + "
          />
        </div>
      </div>
      <div className="container-informacoes">
        <div className="wrapper-link-pages">
          <div className="link-page-institucional">
            <h1 className="page-institucional-title">Institucional</h1>
            <p>Quem Somos</p>
            <p>Política de Privacidade</p>
            <p>Segurança</p>
            <p>Seja um Revendedor</p>
          </div>

          <div className="link-page-duvidas">
            <h1>Dúvidas</h1>
            <p>Entrega</p>
            <p>Pagamento</p>
            <p>Trocas e Devoluções</p>
            <p>Dúvidas Frequentes</p>
          </div>

          <div className="link-page-faleconosco">
            <h1>Fale Conosco</h1>
            <h2>Atendimento ao Consumdor</h2>
            <p>(11) 4159-9504 </p>
            <h2>Atendimento Online</h2>
            <p>(11) 99433-8825</p>
          </div>
        </div>

        <div className="container-midias">
          <div className="wrapper-midias">
            <a href="https://www.facebook.com/">
              <img src={Facebook} alt="Facebook" />
            </a>
            <a href="https://www.instagra.com/">
              <img className="img-instagram" src={Instagram} alt="Instagram" />
            </a>
            <a href="https://www.twitter.com/">
              <img src={Twitter} alt="Twitter" />
            </a>
            <a href="https://www.youtube.com/channel/UCW4o86gZG_ceA8CmHltXeXA">
              <img className="img-youtube" src={Youtube} alt="Youtube" />
            </a>
            <a href="https://www.linkedin.com/company/m3ecommerce/?originalSubdomain=br">
              <img src={Linkedin} alt="Linkedin" />
            </a>
            <p>www.loremimpsum.com</p>
          </div>
        </div>
      </div>
      <Copyright />
    </div>
  );
};

export { Information };
