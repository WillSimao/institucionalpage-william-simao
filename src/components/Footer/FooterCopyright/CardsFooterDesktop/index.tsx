import React from "react";

import "./styles.css";

import Master from "../../../../assets/ImagesFooter/mastercard.svg";
import Visa from "../../../../assets/ImagesFooter/visa.svg";
import AmericaEx from "../../../../assets/ImagesFooter/americaExpress.svg";
import Elo from "../../../../assets/ImagesFooter/elo.svg";
import Hipecard from "../../../../assets/ImagesFooter/hipercard.svg";
import Paypal from "../../../../assets/ImagesFooter/paypal.svg";
import Boleto from "../../../../assets/ImagesFooter/boleto.svg";


const CardsFooter = () => {
    return (
        <div className="border">
            <img src={Master} alt="Cartão Master"  />
            <img src={Visa} alt="Cartão Visa"/>
            <img src={AmericaEx} alt="Cartão America Express"/>
            <img src={Elo} alt="Cartão Elo"/>
            <img src={Hipecard} alt="Cartão Hipecard"/>
            <img src={Paypal} alt="Cartão Paypal"/>
            <img className="wrapper-cards-boleto" src={Boleto} alt="Boleto"/>
        </div>
    )
}

export { CardsFooter };