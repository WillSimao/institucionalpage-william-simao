import React from "react";

import "./styles.css";

import LogoM3 from "../../../../assets/ImagesFooter/img.svg"

const  M3Vtex = () => {
    return (
        <div className="container-logo-m3" >
            <img src={LogoM3} alt="logo m3 vtex" />
        </div>
    )
}


export { M3Vtex };