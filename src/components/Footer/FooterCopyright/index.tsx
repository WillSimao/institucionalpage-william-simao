import React from "react";

import "./styles.css";

import { CardsFooter } from "./CardsFooterDesktop/index";
import { M3Vtex } from "./M3Vtex/index";
import VtexMobile from "../../../assets/ImagesFooter/vtexM3Mobile.svg";
import Vtex from "../../../assets/ImagesFooter/vtex.svg";

import Master from "../../../assets/ImagesFooter/masterMobile.svg";
import Visa from "../../../assets/ImagesFooter/visaMobile.svg";
import AmericaEx from "../../../assets/ImagesFooter/americaExpressMobile.svg";
import Elo from "../../../assets/ImagesFooter/eloMobile.svg";
import Hipecard from "../../../assets/ImagesFooter/hipercardMobile.svg";
import Paypal from "../../../assets/ImagesFooter/paypalMobile.svg";
import Boleto from "../../../assets/ImagesFooter/boletoMobile.svg";
const Copyright = () => {
  return (
    <div className="container-copyright">
      <div className="wrapper-copyright-paragrafo">
        <p className="copyright-paragrafo">
          Lorem ipsum dolor sit amet, consectetur adipiscing
          <br />
          elit, sed do eiusmod tempor
        </p>
      </div>
      <div className="cards-wrapper-mobile">
        <img src={Master} alt="Cartão Master" />
        <img src={Visa} alt="Cartão Visa" />
        <img src={AmericaEx} alt="Cartão America Express" />
        <img src={Elo} alt="Cartão Elo" />
        <img src={Hipecard} alt="Cartão Hipecard" />
        <img src={Paypal} alt="Cartão Paypal" />
        <img className="wrapper-cards-boleto" src={Boleto} alt="Boleto" />
        <div className="divisor"></div>
        <a
          className="anchor-vtex-mobile"
          href="https://vtex.com/br-pt/overview-plataforma/?utm_source=google&utm_medium=cpc&utm_campaign=BR_VTEX_Search_Branded&utm_term=vtex&utm_content=vtex_523171640535"
        >
          <img src={VtexMobile} alt="Plataforma Vtex.io" />
        </a>
      </div>

      <div className="wrapper-cards-footer">
        <CardsFooter />
      </div>
      <a
        className="anchor-vtex-desktop"
        href="https://vtex.com/br-pt/overview-plataforma/?utm_source=google&utm_medium=cpc&utm_campaign=BR_VTEX_Search_Branded&utm_term=vtex&utm_content=vtex_523171640535"
      >
        <img src={Vtex} alt="Plataforma Vtex.io" />
      </a>
      <p className="text-footer-mobile">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      </p>
      <M3Vtex />
    </div>
  );
};

export { Copyright };
