import React from "react";

import "./styles.css";

const SegurancaEPrivacidade = () => {
    return (
        <div className="card-paragraph-container-seguranca">
            <h1 className="card-paragraph-title-seguranca" >Segurança e Privacidade</h1>

            <p className="card-paragraph-one-seguranca">Donec vel purus diam. Pellentesque ullamcorper libero quis elit fringilla, et ultricies felis ullamcorper. 
                Fusce et fringilla nunc, at pellentesque lacus. Nunc gravida ipsum a quam bibendum semper. Suspendisse malesuada pretium nunc at pretium. Sed rutrum luctus risus.
                Fusce diam massa, rhoncus eget posuere et, consectetur ac nulla.</p>

            <p className="card-paragraph-two-seguranca">Suspendisse mollis quam feugiat quam tempor, sed elementum tortor porta. Donec pretium ut metus nec rutrum.
                Sed finibus magna non tempus commodo. Sed facilisis euismod efficitur. Etiam eu rutrum enim. Quisque vulputate neque a lorem congue congue. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                In convallis, enim vitae imperdiet maximus, erat neque molestie nulla, quis elementum massa ipsum a neque.</p>

        </div>
        
    )   
};



export { SegurancaEPrivacidade };