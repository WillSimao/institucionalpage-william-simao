import React from "react";

import "./styles.css";

const FormaDePagamento = () => {
    return (
        <>
            <div className="card-paragraph-container-pagamento" >

                <h1 className="card-paragraph-title-pagamento" >Forma De Pagamento</h1>

                <p className="card-paragraph-one-pagamento" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum a velit ac libero volutpat congue nec id metus. Quisque augue enim, pharetra a tempus nec, feugiat in erat. Praesent ac lacus sed lectus bibendum pulvinar. Aenean mattis libero arcu, a sodales nunc congue ac. Integer porta sit amet sapien et luctus. Curabitur lobortis vitae justo sed maximus. In gravida nulla scelerisque, vestibulum urna quis, tincidunt turpis. Quisque ultrices nunc sed accumsan sodales. 
                Cras tincidunt lectus sit amet faucibus posuere. Aenean lacinia vulputate arcu porttitor congue.
                Donec auctor aliquam lectus, eu eleifend turpis tincidunt quis.</p>

                <p className="card-paragraph-two-pagamento" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum a velit ac libero volutpat congue nec id metus. Quisque augue enim, pharetra a tempus nec, feugiat in erat. Praesent ac lacus sed lectus bibendum pulvinar. Aenean mattis libero arcu, a sodales nunc congue ac. Integer porta sit amet sapien et luctus. Curabitur lobortis vitae justo sed maximus. In gravida nulla scelerisque, vestibulum urna quis, tincidunt turpis. Quisque ultrices nunc sed accumsan sodales. 
                Cras tincidunt lectus sit amet faucibus posuere. Aenean lacinia vulputate arcu porttitor congue.
                Donec auctor aliquam lectus, eu eleifend turpis tincidunt quis.</p>

            </div>
        </>
    )
};


export { FormaDePagamento };