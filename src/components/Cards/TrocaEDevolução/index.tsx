import React from "react";

import "./styles.css";

const TrocaEDevolucao = () => {
    return (
        <div  className="container-card-paragraph-troca" >
            <h1 className="card-paragraph-title-troca" >Troca e Devolução</h1>
            <p className="card-paragraph-one-troca" >Duis molestie nibh gravida, scelerisque dolor et, cursus metus. Vestibulum viverra aliquam ipsum vitae pharetra. Pellentesque interdum lacus varius elit dictum, nec faucibus libero pharetra. Aliquam massa nisl, scelerisque sit amet lectus ut, pellentesque porta purus. In porta arcu a ante tempor, et mattis erat pretium. Aliquam volutpat, turpis sit amet rhoncus rutrum, turpis arcu efficitur turpis, et commodo nunc felis ac enim. Integer blandit varius lorem in dictum. Cras faucibus eleifend consectetur. Phasellus tortor est, euismod sit amet tortor fringilla, laoreet commodo leo. 
            Etiam efficitur lorem at ex ornare bibendum. Morbi sit amet gravida metus. Donec elementum est nunc, ut ultrices mi auctor in.</p>
        </div>
    )

};


export { TrocaEDevolucao };