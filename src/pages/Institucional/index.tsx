import React from "react";

import { BreadCrumbs } from "../../components/BreadCrumbs/index";
import { CardSelect } from "../../components/CardSelect/index";




import "./styles.css";

const Institutional = () => {

    return (
    <div className="wrapper">
        <div className="institucional-container">
        <BreadCrumbs/>
        <h1 className="institucional-titulo">Institucional</h1>            
            <CardSelect/>
        </div>
        

    </div>
    )
}




export { Institutional };