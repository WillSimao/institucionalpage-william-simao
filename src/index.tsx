import React from "react";
import ReactDOM from "react-dom/client";
import "./global.css";

import { Header } from "./components/HeaderPage/index";
import { Menu } from "./components/HeaderPage/Menu/index";
import { Institutional } from "./pages/Institucional/index";
import { Footer } from "./components/Footer/index";

function App() {
  return (
    <>
      <Header />
      <Menu />
      <Institutional />
      <Footer/>
    </>
  );
}

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(<App />);
